FROM tensorflow/tensorflow:latest
EXPOSE 5000
COPY . /app
RUN pip install --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --trusted-host pypi.org -r /app/req.txt
RUN cd /app
CMD python /app/server.py