from flask import Flask
from get_network import get_network
import neupy
print(neupy.__doc__)
app = Flask(__name__)

network = get_network()

@app.route('/<int:p1>/<string:p2>/<string:p3>/<string:p4>/<float:p5>')

def show_med_data(p1, p2, p3, p4, p5):
    # show the subpath after /path/
    new_Data = [p1, p2, p3, p4, p5]
    return F"p1 = {p1}<br>" \
           F"p2 = {p2}<br>" \
           F"p3 = {p3}<br>" \
           F"p4 = {p4}<br>" \
           F"p5 = {p5}<br>"



app.run("0.0.0.0", port=5000)
# http://127.0.0.1:5000/2/F/HIGH/HIGH/25.355