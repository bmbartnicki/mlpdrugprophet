import neupy
from neupy import algorithms, layers, plots
import sklearn.model_selection
from data_normalisation_scaler import DataNormalisationScaler
from data_supplier import DataSupplier


def get_network(*, train_size=0.85, epochs=125, inside_layers=[250, 250]):
    data_supplier = DataSupplier()
    data_supplier.load()
    data, target = data_supplier.data_and_target()
    data_normalization = DataNormalisationScaler(data)
    data = data_normalization.transform(data)

    # environment.reproducible()
    input_size, output_size = data_supplier.sizes()

    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(
        data, target, train_size=train_size
    )

    cgnet = neupy.algorithms.ConjugateGradient(
        network=[
            neupy.layers.Input(input_size),
            *[neupy.layers.Sigmoid(layer) for layer in inside_layers],
            neupy.layers.Sigmoid(output_size),
        ],
        show_epoch=25,
        verbose=True,
    )

    cgnet.train(x_train, y_train, x_test, y_test, epochs=epochs)
    cgnet.plot_errors()
    return cgnet
import pickle
pickle.dump(get_network(), open("net.pickle", "wb"))