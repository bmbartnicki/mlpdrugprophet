from sklearn.preprocessing import MinMaxScaler


class DataNormalisationScaler:
    MAPPER = {'F': 0,
              'M': 1,
              'HIGH': 1,
              'LOW': 0,
              'NORMAL': 0.5}

    def __init__(self, data):
        self.scaler = MinMaxScaler()
        self.scaler.fit(self._apply_mapping(data.to_numpy()))

    def transform(self, data):
        return self.scaler.transform(self._apply_mapping(data.to_numpy()))

    def _apply_mapping(self, data):
        return [
            [self.MAPPER[elem] if elem in self.MAPPER else elem
             for elem in row]
            for row in data
        ]



