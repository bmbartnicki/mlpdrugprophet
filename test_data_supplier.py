import pytest
from .data_supplier import DataSupplier


def test_data_supplier_can_load_data():
    data_supplier = DataSupplier()
    data_supplier.load()
    assert len(data_supplier.data) != 0


def test_data_supplier_can_provide_unique_drug_names():
    data_supplier = DataSupplier()
    data_supplier.load()
    assert set(data_supplier.unique_drugs_names()) == {'DrugY', 'drugC', 'drugX', 'drugA', 'drugB'}


def test_can_get_data_and_train_from_data_supplier():
    data_supplier = DataSupplier()
    data_supplier.load()
    data, target = data_supplier.data_and_target()
    assert len(data.columns) == 5
    for t in target:
        assert t in [[0, 0, 1], [0, 1, 0], [1, 0, 0]]

