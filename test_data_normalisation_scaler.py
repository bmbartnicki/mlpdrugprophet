import pytest
import sys
import pathlib
sys.path.append(str(pathlib.Path(__file__).parent.absolute()))

from data_normalisation_scaler import DataNormalisationScaler


def test_can_create_data_normalsation_scaler_and_transform():
    data_normalisation_scaler = DataNormalisationScaler([[0, 0], [1, 0.1], [2, 0.1]])
    result = [list(subarr) for subarr in data_normalisation_scaler.transform([[-1, 2], [-0.5, 6]])]
    assert result == [[-0.5, 20], [-0.25, 60]]


def test_can_create_data_normalsation_scaler_with_textual_ordered_column():
    data_normalisation_scaler = DataNormalisationScaler([['F', 'HIGH'],
                                                         ['M', 'LOW'],
                                                         ['F', 'NORMAL']])
    result = [list(subarr) for subarr in data_normalisation_scaler.transform([['M', 'LOW'], ['F', 'HIGH']])]
    assert result == [[1, 0], [0, 1]]